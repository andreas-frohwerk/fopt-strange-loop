Strange Loop - Übung
====================
Die Übung und der Entwurf stammen von [Prof. Dr. Rainer Oechsle](https://www.hochschule-trier.de/informatik/rainer-oechsle/). Die Klasse StrangeLoop wurde modifiziert um den Worst-Case zu demonstrieren (durch erzwungenes Wechseln zwischen den beiden Threads)
  
Mögliche Ergebnisse des ursprünglichen Codes: 2-20 (Ursache: Lost-Update Problem)

Extremfall
==========
Jeder Thread bekommt nur ein Update durch und verliert alle anderen Updates:

```
1. Thread-1 | 1.Durchlauf: data.getValue()                                    | local = 0
2. Thread-2 | 9 Durchläufe ohne Kontextwechsel                                | data.value = 9
3. Thread-1 | 1.Durchlauf: local++; data.setValue(local)                      | data.value = 1
4. Thread-2 | 10.Durchlauf: data.getValue()                                   | local = 1 wegen Schritt 3
5. Thread-1 | 8 weitere Durchläufe ohne Kontextwechsel (Thread 1 ist fertig)  | data.value = 9
6. Thread-2 | 10.Durchlauf: local++; data.setValue(local)                     | data.value = 2 wegen Schritt 4
```

Trace
=====
```
[Thread-1] int local = data.getValue() | data.value =  0 | local =  0 | i =  1
--- context switch -----------------------------------------------------------
[Thread-2] int local = data.getValue() | data.value =  0 | local =  0 | i =  1
[Thread-2] local++                     | data.value =  0 | local =  1 | i =  1
[Thread-2] data.setValue(local)        | data.value =  1 | local =  1 | i =  1
Thread 2 keeps going...
Thread 2 keeps going...
Thread 2 keeps going...
[Thread-2] int local = data.getValue() | data.value =  8 | local =  8 | i =  9
[Thread-2] local++                     | data.value =  8 | local =  9 | i =  9
[Thread-2] data.setValue(local)        | data.value =  9 | local =  9 | i =  9
--- context switch -----------------------------------------------------------
[Thread-1] local++                     | data.value =  9 | local =  1 | i =  1
[Thread-1] data.setValue(local)        | data.value =  1 | local =  1 | i =  1
--- context switch -----------------------------------------------------------
[Thread-2] int local = data.getValue() | data.value =  1 | local =  1 | i = 10
--- context switch -----------------------------------------------------------
[Thread-1] int local = data.getValue() | data.value =  1 | local =  1 | i =  2
[Thread-1] local++                     | data.value =  1 | local =  2 | i =  2
[Thread-1] data.setValue(local)        | data.value =  2 | local =  2 | i =  2
Thread 1 keeps going...
Thread 1 keeps going...
Thread 1 keeps going...
[Thread-1] int local = data.getValue() | data.value =  9 | local =  9 | i = 10
[Thread-1] local++                     | data.value =  9 | local = 10 | i = 10
[Thread-1] data.setValue(local)        | data.value = 10 | local = 10 | i = 10
--- context switch -----------------------------------------------------------
[Thread-2] local++                     | data.value = 10 | local =  2 | i = 10
[Thread-2] data.setValue(local)        | data.value =  2 | local =  2 | i = 10
```
