package de.frohwerk.fopt.strange;

import java.util.concurrent.TimeUnit;

public class StrangeLoopRunner {
    public static void main(String[] args) {
//        final int iterations = 1_000_000;
        final int iterations = 1;

        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;

        final int[] resultCounters = new int[40];

        for (int i = 0; i < iterations; i++) {
            final SimpleIntegerValue simpleIntegerValue = new SimpleIntegerValue();

            final StrangeLoop first = new StrangeLoop("Thread-1", simpleIntegerValue);
            final StrangeLoop second = new StrangeLoop("Thread-2", simpleIntegerValue);

            first.start();
            try { TimeUnit.MILLISECONDS.sleep(100); } catch (InterruptedException ignored) { }
            second.start();

            try {
                second.join();
                first.join();
            } catch (InterruptedException e) {
                // Accept interrupt
            }

            final int result = simpleIntegerValue.getValue();

            if (0 <= result && result <= resultCounters.length) {
                resultCounters[result]++;
            } else {
                System.out.printf("Result outside expected parameters: %d%n", result);
            }

            if (result < min) min = result;
            if (result > max) max = result;

            if (i % 10_000 == 0) {
                System.out.printf("%nAfter %d iterations: min = %d | max = %d%n", i, min, max);
                System.out.printf("+----+------------+%n");
                for (int j = min; j <= max; j++) {
                    System.out.printf("| %2d | %10d |%n", j, resultCounters[j]);
                }
                System.out.printf("+----+------------+%n%n");
            }
        }

        System.out.printf("After %d iterations: min = %d | max = %d%n", iterations, min, max);
    }
}
