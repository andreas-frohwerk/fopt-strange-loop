package de.frohwerk.fopt.strange;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

class SimpleIntegerValue {
    private static final Logger logger = LogManager.getLogger(SimpleIntegerValue.class);

    private int value;

    public synchronized int getValue() {
        final int value = this.value;
        logger.info("getValue(): {}", value);
        return value;
    }

    public synchronized void setValue(int value) {
        logger.info("setValue(): {}", value);
        this.value = value;
    }
}

public class StrangeLoop extends Thread {
    private static final Logger logger = LogManager.getLogger(StrangeLoop.class);

    private final SimpleIntegerValue data;

    private static final Object globalMonitor = new Object();

    public StrangeLoop(String name, SimpleIntegerValue data) {
        super(name);
        this.data = data;
    }

    public void run() {
        for (int i = 1; i <= 10; i++) {
            if ("Thread-1".equals(getName()) && i == 2) swap("before read", i);
            if ("Thread-2".equals(getName()) && i == 10) swap("before read", i);
            int local;
            synchronized (data) {
                local = data.getValue();
                dump("int local = data.getValue()", local, i);
            }
            if ("Thread-2".equals(getName()) && i == 10) swap("before update", i);
            if ("Thread-1".equals(getName()) && i == 1) swap("before update", i);
            synchronized (data) {
                local++;
                dump("local++", local, i);
            }
            synchronized (data) {
                data.setValue(local);
                dump("data.setValue(local)", local, i);
            }
        }
        System.out.println("--- context switch -----------------------------------------------------------");
        signal(() -> {});
    }

    private void dump(final String descriptor, final int local, final int i) {
        System.out.printf("[%s] %-27s | data.value = %2d | local = %2d | i = %2d%n", getName(), descriptor, data.getValue(), local, i);
    }

    private static void signal(final Runnable runnable) {
        synchronized (globalMonitor) {
            runnable.run();
            globalMonitor.notify();
        }
    }

    private void swap(String description, int iteration) {
        synchronized (globalMonitor) {
            globalMonitor.notify();
            while (true) {
                try {
                    System.out.println("--- context switch -----------------------------------------------------------");
                    globalMonitor.wait();
                    break;
                } catch (InterruptedException e) {
                    // Should never happen in this example
                    e.printStackTrace();
                }
            }
        }
    }
}
